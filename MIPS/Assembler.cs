﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CSharpUtils;
using CSharpUtils.Streams;
using CSharpUtils.Arrays;


namespace MIPS
{
    public class Assembler
    {

        public static int ParseIntegerConstant(String value)
        {
            return Int32.Parse(value);
        }

    }
}
