﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Math;

using System.IO;

namespace ELFParser
{
    public class SFO: IDisposable
    {
        private Stream sfo;

        public SFO(Stream sfo)
        {
            this.sfo = sfo;
        }

        public bool CheckMagic()
        {
            byte[] magic = { 0x00, 0x50, 0x53, 0x46};

            byte[] check = new byte[4];

            sfo.Read(check, 0, 4);

            if (ByteArrays.ByteArrayCompare(magic, check))
                return true;
            else
                return false;
        }

        public void Dispose()
        {
            sfo.Close();
        }

    }
}
