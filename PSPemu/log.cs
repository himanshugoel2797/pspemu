﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PSPemu
{
    public partial class log : Form
    {

        public static log logger = new log();

        public log()
        {
            InitializeComponent();
            
        }

        public void writeError(String message)
        {
            string text = "[" + DateTime.Now.ToLocalTime().ToShortTimeString() + "]: " + message + Environment.NewLine;
            this.richTextBox1.SelectionColor = Color.Red;
            richTextBox1.SelectedText = text;
        }

        public void writeWarning(String message)
        {
            string text = "[" + DateTime.Now.ToLocalTime().ToShortTimeString() + "]: " + message + Environment.NewLine;
            this.richTextBox1.SelectionColor = Color.Orange;
            richTextBox1.SelectedText = text;
        }

        public void writeStatus(String message)
        {
            string text = "[" + DateTime.Now.ToLocalTime().ToShortTimeString() + "]: " + message + Environment.NewLine;
            this.richTextBox1.SelectionColor = Color.Green;
            richTextBox1.SelectedText += text;
        }

    }
}
