﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using DiscUtils.Iso9660;
using ELFParser;
using System.Net;

namespace PSPemu
{
    public partial class MemstickBrowser : Form
    {

        public string FileName { get; private set; }
        public string SafeFileName { get; private set; }
        public Pbp.HeaderStruct header {get; private set;}

        public bool iso { get; set; }

        List<string> filePaths = new List<string>();
        List<string> fileNames = new List<string>();

        public MemstickBrowser(bool iso)
        {
            InitializeComponent();
            this.iso = iso;
            
        }

        public void Init(bool iso)
        {
            this.iso = iso;

            if (iso)
                DirSearch(Path.Combine(Environment.CurrentDirectory), "*.iso");
            else
            {
                DirSearch(Path.Combine(Environment.CurrentDirectory), "*.pbp");
            }
        }

        void DirSearch(string sDir, string format)
        {
            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    foreach (string f in Directory.GetFiles(d, format))
                    {
                        filePaths.Add(f);

                        fileNames.Add(Path.GetFileNameWithoutExtension(f));

                        getName(Path.GetFileNameWithoutExtension(f));
                    }
                    DirSearch(d, format);
                }
            }
            catch (System.Exception excpt)
            {
                log.logger.writeError(excpt.Message);
            }
        }


        string fileSearch(string filename)
        {
            string f = "";

            foreach (string file in filePaths)
            {
                if (file.Contains(filename))
                {
                    f = file;
                }
            }

            return f;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FileName = filePaths[listBox1.SelectedIndex];
            SafeFileName = fileNames[listBox1.SelectedIndex];
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();

        }

        private void getName(string filep)
        {
            FileStream file = new FileStream(fileSearch(filep), FileMode.Open);

            if (iso)
            {
                using (CDReader read = new CDReader(file, false))
                {
                    if (read.FileExists(@"PSP_GAME\PARAM.SFO"))
                    {
                        Stream sfo = read.OpenFile(@"PSP_GAME\PARAM.SFO", FileMode.Open);

                        Psf parser = new Psf(sfo);

                        listBox1.Items.Add(parser.EntryDictionary["TITLE"].ToString());

                        sfo.Dispose();
                    }
                }
            }
            else
            {
                Pbp pbp = new Pbp();
                pbp.Load(file);
                Psf parser = new Psf(pbp[Pbp.Types.ParamSfo]);
                listBox1.Items.Add(parser.EntryDictionary["TITLE"].ToString());

            }

            file.Dispose();
        }

        private void UMD()
        {

            if (!Directory.Exists(@"Temp\ISO\" + FileName))
            {
                Directory.CreateDirectory(@"Temp\ISO\" + FileName);
            }

            {
                FileStream file = new FileStream(fileSearch(FileName), FileMode.Open);
                using (CDReader read = new CDReader(file, false))
                {
                    if (read.FileExists(@"PSP_GAME\PARAM.SFO"))
                    {
                        Stream sfo = read.OpenFile(@"PSP_GAME\PARAM.SFO", FileMode.Open);

                        Psf parser = new Psf(sfo);

                        sfo.Dispose();
                    }
                    if (read.FileExists(@"PSP_GAME\PIC0.PNG"))
                    {
                        using (Stream ico0 = read.OpenFile(@"PSP_GAME\PIC0.PNG", FileMode.Open))
                        {
                            byte[] data = new byte[ico0.Length];
                            ico0.Read(data, 0, data.Length);
                            File.WriteAllBytes(@"Temp\ISO\" + FileName + @"\PIC0.PNG", data);
                        }

                        pictureBox1.CreateGraphics().DrawImage(Image.FromFile(@"Temp\ISO\" + FileName + @"\PIC0.PNG"), 0, 10);

                    }
                    else
                    {
                        pictureBox1.CreateGraphics().FillRectangle(Brushes.LightGray, 0, 0, 480, 272);
                    }

                    if (read.FileExists(@"PSP_GAME\ICON0.PNG"))
                    {
                        using (Stream ico0 = read.OpenFile(@"PSP_GAME\ICON0.PNG", FileMode.Open))
                        {
                            byte[] data = new byte[ico0.Length];
                            ico0.Read(data, 0, data.Length);
                            File.WriteAllBytes(@"Temp\ISO\" + FileName + @"\ICON0.PNG", data);
                        }

                        pictureBox1.CreateGraphics().DrawImage(Image.FromFile(@"Temp\ISO\" + FileName + @"\ICON0.PNG"), 30, 153);

                    }
                    else
                    {
                        pictureBox1.CreateGraphics().DrawImageUnscaled(Image.FromFile(@"Temp\ISO\icon0.png"), 30, 153);
                    }
                }
                file.Dispose();
            }

        }

        private void PBP()
        {


            Pbp eboot = new Pbp();
            Pbp pbp = eboot.Load(File.OpenRead(fileSearch(FileName)));


            try
            {
                pictureBox1.CreateGraphics().FillRectangle(Brushes.LightGray, 0, 0, 480, 272);
                pictureBox1.CreateGraphics().DrawImage(Image.FromStream(pbp[Pbp.Types.Pic1Png]), 0, 10);
            }
            catch (Exception)
            {

#if !DEBUG
                log.logger.writeWarning(er.Message);
#endif
            }

            //Ignore any errors because most likely the image is just not present
            try
            {
                pictureBox1.CreateGraphics().DrawImageUnscaled(Image.FromStream(pbp[Pbp.Types.Icon0Png]), 30, 153);
            }
            catch (Exception)
            {
                pictureBox1.CreateGraphics().DrawImageUnscaled(Image.FromFile(@"Temp\ISO\icon0.png"), 30, 153);
#if !DEBUG
                log.logger.writeWarning(er.Message);
#endif
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
#if !DEBUG
            try
#endif
            {
                FileName = fileNames[listBox1.SelectedIndex];

                Text = "Memstick Browser: " + listBox1.GetItemText(listBox1.SelectedItem);
                if (iso)
                {
                    UMD();
                }
                else
                {
                    PBP();
                }
            }
#if !DEBUG
            catch (exception error)
            {
                log.logger.writeerror(error.message + " " + error.source + " " + error.targetsite);
            }
#endif
        }
    }
}
