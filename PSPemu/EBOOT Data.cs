﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ELFParser;

namespace PSPemu
{
    public partial class EBOOT_Data : Form
    {
        public Pbp.HeaderStruct data;
        public EBOOT_Data()
        {
            InitializeComponent();

            
        }

        private void EBOOT_Data_Load(object sender, EventArgs e)
        {
            for (int index = 0; index < data.Offsets.Length; index++)
            {
                listView1.Items[index].SubItems.Add(data.Offsets[index].ToString("X8"));
            }
        }
    }
}
