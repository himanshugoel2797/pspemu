﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CPU;

namespace PSPemu
{
    public partial class Form1 : Form
    {

        private static Form1 form = new Form1();
        MemstickBrowser open = new MemstickBrowser(false);

        public static Form1 getInstance()
        {
            return form;
        }




        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            log.logger.Show();
            log.logger.writeStatus("test!");
            log.logger.writeError("test!");
            log.logger.writeWarning("text!");

            //Initialize the CPU
            CPU.Registers.regs.Reset();
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (logToolStripMenuItem.Checked) log.logger.Show();
            else log.logger.Hide();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void fileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                globals.filename = openFileDialog1.FileName;
                Memory.LoadELF(openFileDialog1.FileName);
            }
        }

        private void memstickToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void iSOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            open.Init(true);

            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
            }
        }

        private void homebrewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            open.Init(false);

            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
            }
        }

        private void showELFHeaderInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EBOOT_Data elfInfo = new EBOOT_Data();
            elfInfo.data = open.header;
            elfInfo.Show();
        }

        private void debuggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CPU.Debugger debug = new CPU.Debugger();
            debug.Show();
        }
    }
}
