﻿namespace CPU
{
    partial class Debugger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem67 = new System.Windows.Forms.ListViewItem("$zr");
            System.Windows.Forms.ListViewItem listViewItem68 = new System.Windows.Forms.ListViewItem("$at");
            System.Windows.Forms.ListViewItem listViewItem69 = new System.Windows.Forms.ListViewItem("$v0");
            System.Windows.Forms.ListViewItem listViewItem70 = new System.Windows.Forms.ListViewItem("$v1");
            System.Windows.Forms.ListViewItem listViewItem71 = new System.Windows.Forms.ListViewItem("$a0");
            System.Windows.Forms.ListViewItem listViewItem72 = new System.Windows.Forms.ListViewItem("$a1");
            System.Windows.Forms.ListViewItem listViewItem73 = new System.Windows.Forms.ListViewItem("$a2");
            System.Windows.Forms.ListViewItem listViewItem74 = new System.Windows.Forms.ListViewItem("$a3");
            System.Windows.Forms.ListViewItem listViewItem75 = new System.Windows.Forms.ListViewItem("$t0");
            System.Windows.Forms.ListViewItem listViewItem76 = new System.Windows.Forms.ListViewItem("$t1");
            System.Windows.Forms.ListViewItem listViewItem77 = new System.Windows.Forms.ListViewItem("$t2");
            System.Windows.Forms.ListViewItem listViewItem78 = new System.Windows.Forms.ListViewItem("$t3");
            System.Windows.Forms.ListViewItem listViewItem79 = new System.Windows.Forms.ListViewItem("$t4");
            System.Windows.Forms.ListViewItem listViewItem80 = new System.Windows.Forms.ListViewItem("$t5");
            System.Windows.Forms.ListViewItem listViewItem81 = new System.Windows.Forms.ListViewItem("$t6");
            System.Windows.Forms.ListViewItem listViewItem82 = new System.Windows.Forms.ListViewItem("$t7");
            System.Windows.Forms.ListViewItem listViewItem83 = new System.Windows.Forms.ListViewItem("$s0");
            System.Windows.Forms.ListViewItem listViewItem84 = new System.Windows.Forms.ListViewItem("$s1");
            System.Windows.Forms.ListViewItem listViewItem85 = new System.Windows.Forms.ListViewItem("$s2");
            System.Windows.Forms.ListViewItem listViewItem86 = new System.Windows.Forms.ListViewItem("$s3");
            System.Windows.Forms.ListViewItem listViewItem87 = new System.Windows.Forms.ListViewItem("$s4");
            System.Windows.Forms.ListViewItem listViewItem88 = new System.Windows.Forms.ListViewItem("$s5");
            System.Windows.Forms.ListViewItem listViewItem89 = new System.Windows.Forms.ListViewItem("$s6");
            System.Windows.Forms.ListViewItem listViewItem90 = new System.Windows.Forms.ListViewItem("$s7");
            System.Windows.Forms.ListViewItem listViewItem91 = new System.Windows.Forms.ListViewItem("$t8");
            System.Windows.Forms.ListViewItem listViewItem92 = new System.Windows.Forms.ListViewItem("$t9");
            System.Windows.Forms.ListViewItem listViewItem93 = new System.Windows.Forms.ListViewItem("$k0");
            System.Windows.Forms.ListViewItem listViewItem94 = new System.Windows.Forms.ListViewItem("$k1");
            System.Windows.Forms.ListViewItem listViewItem95 = new System.Windows.Forms.ListViewItem("$gp");
            System.Windows.Forms.ListViewItem listViewItem96 = new System.Windows.Forms.ListViewItem("$sp");
            System.Windows.Forms.ListViewItem listViewItem97 = new System.Windows.Forms.ListViewItem("$fp");
            System.Windows.Forms.ListViewItem listViewItem98 = new System.Windows.Forms.ListViewItem("$ra");
            System.Windows.Forms.ListViewItem listViewItem99 = new System.Windows.Forms.ListViewItem("$pc");
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.rAMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dumpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.richTextBox1.DetectUrls = false;
            this.richTextBox1.Location = new System.Drawing.Point(14, 113);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(478, 510);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem67,
            listViewItem68,
            listViewItem69,
            listViewItem70,
            listViewItem71,
            listViewItem72,
            listViewItem73,
            listViewItem74,
            listViewItem75,
            listViewItem76,
            listViewItem77,
            listViewItem78,
            listViewItem79,
            listViewItem80,
            listViewItem81,
            listViewItem82,
            listViewItem83,
            listViewItem84,
            listViewItem85,
            listViewItem86,
            listViewItem87,
            listViewItem88,
            listViewItem89,
            listViewItem90,
            listViewItem91,
            listViewItem92,
            listViewItem93,
            listViewItem94,
            listViewItem95,
            listViewItem96,
            listViewItem97,
            listViewItem98,
            listViewItem99});
            this.listView1.Location = new System.Drawing.Point(498, 30);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(235, 593);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Registers";
            this.columnHeader1.Width = 104;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Values";
            this.columnHeader2.Width = 116;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rAMToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(745, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // rAMToolStripMenuItem
            // 
            this.rAMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dumpToolStripMenuItem});
            this.rAMToolStripMenuItem.Name = "rAMToolStripMenuItem";
            this.rAMToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.rAMToolStripMenuItem.Text = "RAM";
            // 
            // dumpToolStripMenuItem
            // 
            this.dumpToolStripMenuItem.Name = "dumpToolStripMenuItem";
            this.dumpToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.dumpToolStripMenuItem.Text = "Dump";
            this.dumpToolStripMenuItem.Click += new System.EventHandler(this.dumpToolStripMenuItem_Click);
            // 
            // Debugger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 635);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Debugger";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Debugger";
            this.Load += new System.EventHandler(this.Debugger_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem rAMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dumpToolStripMenuItem;
    }
}