﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPU
{
    public partial class Debugger : Form
    {
        public Debugger()
        {
            InitializeComponent();
        }

        private void Debugger_Load(object sender, EventArgs e)
        {
            for (int index = 0; index < 33; index++)
            {
                listView1.Items[index].SubItems.Add(Registers.regs[index].ToString("X8"));
            }
        }

        private void dumpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Memory.Dump("dump.bin");
        }
    }
}
