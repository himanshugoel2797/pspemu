﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace CPU
{
    public class Memory
    {

        /// Start       End         Size  Description
        /// 0x00010000  0x00013fff  16kb  Scratchpad
        /// 0x04000000  0x041fffff  2mb   Video Memory / Frame Buffer
        /// 0x08000000  0x09ffffff  32mb  Main Memory
        /// 0x1c000000  0x1fbfffff        Hardware I/O
        /// 0x1fc00000  0x1fcfffff  1mb   Hardware Exception Vectors (RAM)
        /// 0x1fd00000  0x1fffffff        Hardware I/O
        /// 

        static byte[] temp = new byte[1];


        public static bool Fat =true;

        public static int MemSize = 32 * 1048576;
        public static  int FrameBufferSize = 2 * 0x100000;
        public static  int ScratchPadSize = 4 * 1024;

        public static  int HardwareIO1Size = 59 * 1024 * 1024;  //59 KB
        public static  int HardwareIO2Size = 2 * 1024 * 1024; //2 kB
        public static  int HardwareExceptionSize = 1024 * 1023;

        public static  int FrameBufferOffset = 0x04000000;
        public static  int ScratchPadOffset = 0x00010000;
        public static  int MainMemOffset = 0x08000000;

        public static  int HardwareIO1Offset = 0x1C000000;
        public static  int HardwareIO2Offset = 0x1FD00000;
        public static  int HardwareExceptionOffset = 0x1fc00000;

        public static byte[] RAM = new byte[0x09ffffff - MainMemOffset];


        public static void LoadELF(string filename)
        {
            FileStream elf = new FileStream(filename, FileMode.Open);

            elf.Read(RAM, MainMemOffset, (int)elf.Length);

            elf.Dispose();

        }

        public static void Dump(string FileName)
        {
            File.WriteAllBytes(FileName, RAM);
        }

    }
}
