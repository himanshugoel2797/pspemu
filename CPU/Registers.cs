﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU
{
    public class Registers
    {

        public long[] registers = new long[35];

        public static Registers regs = new Registers();


        public enum RegisterVal
        {
            //General purpose Registers
            ZR = 0,  AT = 1, 
            V0 = 2,  V1 = 3,
            A0 = 4,  A1 = 5,
            A2 = 6,  A3 = 7,
            T0 = 8,  T1 = 9,
            T2 = 10, T3 = 11,
            T4 = 12, T5 = 13,
            T6 = 14, T7 = 15,
            S0 = 16, S1 = 17,
            S2 = 18, S3 = 19,
            S4 = 20, S5 = 21,
            S6 = 22, S7 = 23,
            T8 = 24, T9 = 25,
            K0 = 26, K1 = 27,
            GP = 28, SP = 29,
            FP = 30, RA = 31,
            
            //Special Registers
            PC = 32, HI = 33, LO = 34
        }


        public void Reset()
        {
            //Clear the RAM
            Array.Clear(Memory.RAM, 0, Memory.MemSize);

            //Reset all the registers
            for (int index = 0; index < 35; index++)
            {
                registers[index] = 0xDEADBEEF;

            }

            this[RegisterVal.ZR] = 0x0;
            this[RegisterVal.PC] = Memory.MainMemOffset;
        }

        public long this[RegisterVal val]
        {
            get
            {
                return registers[(int)val];
            }

            set
            {
                registers[(int)val] = value;
            }
        }

        public long this[int val]
        {
            get
            {
                return registers[val];
            }

            set
            {
                registers[val] = value;
            }
        }
    }
}
